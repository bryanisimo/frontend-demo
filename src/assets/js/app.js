import $ from 'jquery';
import slick from 'slick-carousel';

const startHeaderListener = () => {
  let _$mainNavigation = $('ul.main-nav');
  $('#burger-button').on('click', event => {
    event.preventDefault();
    _$mainNavigation.addClass('active');
  });
  $('#close-button').on('click', event => {
    event.preventDefault();
    _$mainNavigation.removeClass('active');
  });
  if ($('#nothing')) {
    console.info(slick);
  }
};
const initSlider = () => {
  $('.home-slider').slick({
    dots: true,
    arrows: true
  });
};
startHeaderListener();
initSlider();
// Happy coding from Pixel2HTML
